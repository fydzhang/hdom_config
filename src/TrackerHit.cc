//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file TrackerHit.cc
/// \brief Implementation of the TrackerHit class

#include "TrackerHit.hh"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
// #include "MyAnalysis.hh"
#include "g4csv.hh"
// #include "G4AnalysisManager.hh"
#include <iomanip>
#include "G4SystemOfUnits.hh"
G4ThreadLocal G4Allocator<TrackerHit> *TrackerHitAllocator = 0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TrackerHit::TrackerHit()
    : G4VHit(),
      fType(0),
      cp_num(0),
      fEnergy(0),
      fTime(0),
      fPx(0),
      fPy(0),
      fPz(0)
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TrackerHit::~TrackerHit() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TrackerHit::TrackerHit(const TrackerHit &right)
    : G4VHit()
{
  fTime = right.fTime;
  fEnergy = right.fEnergy;
  fType = right.fType;
  cp_num = right.cp_num;
  fPx = right.fPx;
  fPy = right.fPy;
  fPz = right.fPz;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

const TrackerHit &TrackerHit::operator=(const TrackerHit &right)
{
  fTime = right.fTime;
  fEnergy = right.fEnergy;
  fType = right.fType;
  cp_num = right.cp_num;
  fPx = right.fPx;
  fPy = right.fPy;
  fPz = right.fPz;
  return *this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool TrackerHit::operator==(const TrackerHit &right) const
{
  return (this == &right) ? true : false;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TrackerHit::Print()
{
  auto *analysisManager = G4AnalysisManager::Instance();
  
  analysisManager ->FillNtupleDColumn(0, fType);
  analysisManager ->FillNtupleDColumn(1, cp_num);
  analysisManager ->FillNtupleDColumn(2,fTime / ns);
  analysisManager ->FillNtupleDColumn(3,fEnergy / eV);
  analysisManager ->FillNtupleDColumn(4,fPx / eV);  
  analysisManager ->FillNtupleDColumn(5,fPy / eV);
  analysisManager ->FillNtupleDColumn(6,fPz / eV);
  // record information 
  analysisManager->AddNtupleRow();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
