//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file DetectorConstruction.hh
/// \brief Definition of the DetectorConstruction class

#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "tls.hh"
#include "G4Sphere.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "yaml-cpp/yaml.h"
class G4VPhysicalVolume;
class G4LogicalVolume;
class G4Material;
class G4UserLimits;

/// Detector construction class to define materials, geometry


class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    DetectorConstruction();
    virtual ~DetectorConstruction();

  public:
    virtual G4VPhysicalVolume* Construct();
    virtual void ConstructSDandField();
    void SetCheckOverlaps(G4bool);
    YAML::Node rootNode;
  private:
    // methods
    void DefineMaterials();
    G4VPhysicalVolume* DefineVolumes();
    void SetPSVolumes();

    // define the solid and logic vloume of 
    G4Sphere *solidPmt;
    G4LogicalVolume *logicPmt;
    G4Tubs *solidSipm;
    G4LogicalVolume *logicSipm;
    // data members
    // G4int fNbOfPMTs;                         //number of PMTs
    // G4LogicalVolume**   fLogicPMT;           // pointer to the logical PMT
    G4Material*        Seawater_Material;    // pointer to the seawater material
    G4Material*        Gel_Material;         // optical glue
    G4Material*        Glass_Material;       // glass ball
    G4Material*        Pmt_Material;         // pmt
    G4Material*        Plastic_Material;     // plastic suppoter

    G4bool fCheckOverlaps;                  // option to activate checking of volumes overlaps 
    std::vector<double> theta_array_PMT;  //geometry parameters of theta and phi
    G4int numOftheta_PMT;
    std::vector<std::vector<double>> phi_array_PMT;
    std::vector<double> theta_array_Sipm;
    G4int numOftheta_Sipm;
    std::vector<std::vector<double>> phi_array_Sipm;
    std::vector<double> scaLenMie;
    std::vector<double> scaLenRay;
    std::vector<double> absLen;
    std::vector<double> refracIdx;
    std::vector<double> photonEnergy;
    // std::string fileGeometry;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
